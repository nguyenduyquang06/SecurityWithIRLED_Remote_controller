# IoT Project using Android device and Arduino MCU

## Target: Security with IR LED

# Introduction

- Sử dụng IR led cảnh báo có người xâm nhập thông qua giao diện phần mềm trên Android
- Project được viết bằng Android Java và C++ cho lập trình nhúng

# Algorithm Flowchart

<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/Annotation%202018-12-08%20090309_zpsfly7nois.jpg" />
</p>

# Presentation

<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/Annotation%202018-12-08%20090230_zpsxc5occwh.jpg" />
</p>

